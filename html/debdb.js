$(document).ready(function() {
	$('#machines thead tr').clone(true).appendTo('#machines thead');
	$('#machines thead tr:eq(1) th').each(function (i) {
		var title = $(this).text();
		$(this).html('<input type="text" placeholder="Search \''+title.toLowerCase()+'\'" />');
		$('input', this).on( 'keyup change', function () {
			if (table.column(i).search() !== this.value) {
				table.column(i).search(this.value).draw();
			}
		});
	} );

	var table = $("#machines").DataTable({
		orderCellsTop: true,
		paging: false,
		pageLength: 25,
		lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
	});
	$(".sort").removeAttr("href")
});
