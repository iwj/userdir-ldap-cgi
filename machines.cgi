#!/usr/bin/perl
# $Id: machines.cgi,v 1.12 2006/12/27 23:00:04 rmurray Exp $

# (c) 1999 Randolph Chung. Licensed under the GPL. <tausq@debian.org>
# (c) 2004 Martin Schulze. Licensed under the GPL. <joey@debian.org>
# (c) 2006 Ryan Murray. Licensed under the GPL. <rmurray@debian.org>
# (c) 2008 Martin Zobel-Helas. Licensed under the GPL. <zobel@debian.org>

use lib '.';
use strict vars;
#use Apache::Registry;
use CGI;
use Util;
use Net::LDAP qw(LDAP_SUCCESS LDAP_PROTOCOL_ERROR);
use Fcntl;
use POSIX;
use MIME::Base64;
use Digest::MD5 qw(md5_hex);

my (%attrs, @attrorder, %summaryattrs, @summaryorder, %summarylistitems);

# This defines the description of the fields, and which fields are retrieved
%attrs = ('hostname' => 'Host name',
          'admin' => 'Admin contact',
          'architecture' => 'Architecture',
          'distribution' => 'Distribution',
	  'access' => 'Access',
	  'sponsor' => 'Sponsor',
	  'sponsor-admin' => 'Sponsor admin',
	  'l' => 'Location',
	  'machine' => 'Processor',
	  'memory' => 'Memory',
	  'disk' => 'Disk space',
	  'bandwidth' => 'Bandwidth',
	  'status' => 'Status',
	  'notes' => 'Notes',
	  'sshrsahostkey' => 'SSH host key',
	  'description' => 'Description',
	  'purpose' => 'Purposes of this server',
#	  'createtimestamp' => 'Entry created',
#	  'modifytimestamp' => 'Entry modified'
	 );

# This defines what fields are displayed, and in what order
@attrorder = qw(hostname admin architecture distribution access
                sponsor sponsor-admin l machine memory
	        disk bandwidth status notes sshrsahostkey sshrsahostfprint
		description purpose);

# ditto for summary
%summaryattrs = ('hostname' => 'Host name',
		 'host'     => 'just for a link',
		 'description' => 'Description',
		 'architecture' => 'Architecture',
		 'status' => 'Status',
		 'access' => 'Access',
		 'sponsor' => 'Sponsor',
		 'purpose' => 'Purpose');
@summaryorder = qw{hostname description architecture sponsor purpose status access};
%summarylistitems = map {$_=>1} qw{purpose sponsor};

# Global settings...
my %config = &Util::ReadConfigFile;

my ($ldap, $mesg, $dn, $entries, $data, %output, $key, $hostlist, $hostdetails, $selected, %summary);
sub DieHandler {
  $ldap->unbind if (defined($ldap));
}

sub wiki_link($) {
	my ($in) = @_;
	# [[hostname|text]] makes a link
	# [[hostname]] makes a link too
	# if you add a * after [[ it's still the same, only not used for ssh_known_hosts in ud-generate
	# [[-hostname]] are not links, but get added to known_hosts.  we should drop the [[- ]] tho
	$in =~ s#\[\[-(.*?)\]\]#$1#g;
	$in =~ s#\[\[\*?(.*?)\|(.*?)\]\]#<a href="http://$1">$2</a>#g;
	$in =~ s#\[\[\*?(.*?)\]\]#<a href="http://$1">$1</a>#g;
	return $in;
}

# in the purpose field [[host|some other text]] (where some other text is optional)
# makes a hyperlink on the web thing. we now also add these hosts to the ssh known_hosts
# file.  But so that we don't have to add everything we link we can add an asterisk
# and say [[*... to ignore it.  In order to be able to add stuff to ssh without
# http linking it we also support [[-hostname]] entries.
#
# sponsors are also wikified like purpose.  maybe others as well
sub item_uplist($) {
	my ($items) = @_;
	my $out = undef;
	my(@tmp) = @$items;

	if (scalar @tmp>= 1) {
		$out = "<ul>".
			join("", map { 
				"<li>".wiki_link($_)."</li>\n";
			  } sort {my $A=$a; my $B=$b; $A =~ s/\[\[[-*]?(.*?)\]\]/\1/g; $B =~ s/\[\[[-*]?(.*?)\]\]/\1/g; $A cmp $B} @tmp
			).
			"</ul>";
	}
	return $out;
}

#$SIG{__DIE__} = \&DieHandler;

my $query = new CGI;
my $host = lc($query->param('host'));
my $sortby = lc($query->param('sortby')) || "host";
my $sortorder = lc($query->param('sortorder')) || "asc";


&Util::HTMLSendHeader;
$ldap = Net::LDAP->new($config{ldaphost}) || &Util::HTMLError($!);
&Util::UpgradeConnection($ldap) unless $config{usessl} eq 'False';
$mesg;
$ldap->bind;

$mesg = $ldap->search(base  => $config{hostbasedn}, filter => 'host=*');
$mesg->code && &Util::HTMLError($mesg->error);
$entries = $mesg->as_struct;

foreach $dn (sort {$entries->{$a}->{host}->[0] cmp $entries->{$b}->{host}->[0]} keys(%$entries)) {
  $data = $entries->{$dn};

  my $thishost = $data->{host}->[0];
  $selected = "";
  
  if (lc($thishost) eq $host) {
    $output{havehostdata} = 1;

    foreach $key (keys(%attrs)) {
      $output{$key} = $data->{$key}->[0];
    }
  
    $output{hostname} = undef;
    foreach my $hostname (@{$data->{hostname}}) {
      $output{hostname} .= sprintf("%s%s", ($output{hostname} ? ', ' : ''), $hostname);
    }

    # Modified/created time. TODO: maybe add is the name of the creator/modifier
    $output{modifytimestamp} = &Util::FormatTimestamp($output{modifytimestamp});
    $output{createtimestamp}  = &Util::FormatTimestamp($output{createtimestamp});
    
    # Format email addresses
    if (defined $data->{admin} and scalar($data->{admin}) > 1) {
      my @admins = map { sprintf("<a href=\"mailto:%s\">%s</a>", $_, $_) } @{$data->{admin}};
      $output{admin} = item_uplist(\@admins);
    } else {
      $output{admin} = sprintf("<a href=\"mailto:%s\">%s</a>", $output{admin}, $output{admin})
        if defined $output{admin};
    }
    $output{'sponsor-admin'} = sprintf("<a href=\"mailto:%s\">%s</a>", $output{'sponsor-admin'}, $output{'sponsor-admin'})
      if defined $output{'sponsor-admin'};

    $output{sshrsahostkey} = undef;
    foreach $key (@{$data->{sshrsahostkey}}) {
      $output{sshrsahostkey} .= $key . "<br>";
    }

    my $sponsor = item_uplist($data->{sponsor});
    $output{sponsor} = $sponsor if defined $sponsor;
    my $purpose = item_uplist($data->{purpose});
    $output{purpose} = $purpose if defined $purpose;

    $selected = " selected ";
  }

  $hostlist .= "<option value=\"$thishost\"$selected>$thishost\n" unless ($data->{status}->[0] =~ /^unlisted/);

  # collect summary info
  foreach $key (keys(%summaryattrs)) {
    if (exists $summarylistitems{$key}) {
      my $v = item_uplist($data->{$key});
      $summary{$thishost}{$key} = $v if defined $v;
    } else {
      $summary{$thishost}{$key} = $data->{$key}->[0];
    }
  }

  $summary{$thishost}{hostname} = undef;
  foreach my $hostname (@{$data->{hostname}}) {
    $summary{$thishost}{hostname} .= sprintf("%s<a href=\"machines.cgi?host=%s\">%s</a>", ($summary{$thishost}{hostname} ? '<br>' : ''), $summary{$thishost}{host}, $hostname);
  }
}
$ldap->unbind;

if ($output{havehostdata}) {
  $hostdetails = "<h1>Information about $output{hostname}</h1>\n";
  $hostdetails .= "<ul>\n";
  foreach $key (@attrorder) {
    if ($output{$key}) {
      $hostdetails .= "<li><b>$attrs{$key}</b>: $output{$key}\n";
    }
  }
  $hostdetails .= "</ul>\n";
} else {
  # display summary info
  $hostdetails = "<h1>Summary</h1>\n";
  $hostdetails .= "<table id=\"machines\" class=\"display compact cell-border\" border=\"1\" cellpadding=\"0\" cellspacing=\"1\">\n<thead>\n<tr>";
  foreach $key (@summaryorder) {
    if ($sortby ne $key) {
      $hostdetails .= "<th><a class=\"sort\" href=\"machines.cgi?sortby=$key&sortorder=asc\">$summaryattrs{$key}</a></th>";
    } else {
      if ($sortorder ne "dsc") {
        $hostdetails .= "<th><a class=\"sort\" href=\"machines.cgi?sortby=$key&sortorder=dsc\">$summaryattrs{$key}</a></th>";
      } else {
        $hostdetails .= "<th><a class=\"sort\" href=\"machines.cgi?sortby=$key&sortorder=asc\">$summaryattrs{$key}</a></th>";
      }
    }
  }
  $hostdetails .= "</tr>\n</thead>\n<tbody>\n";
  
  my @sorted;
  if ($sortorder eq "asc") {
     @sorted = sort {($summary{$a}->{$sortby} cmp $summary{$b}->{$sortby}) || ($summary{$a}->{'host'} cmp $summary{$b}->{'host'})} keys(%summary)
  } else {
     @sorted = sort {($summary{$b}->{$sortby} cmp $summary{$a}->{$sortby}) || ($summary{$a}->{'host'} cmp $summary{$b}->{'host'})} keys(%summary)
  }
  foreach $host (@sorted) {
    next if $summary{$host}{status} =~ /^unlisted/;
    $hostdetails .= "<tr>";
    foreach $key (@summaryorder) {
      $hostdetails .= "<td>$summary{$host}{$key}&nbsp;</td>";
    }
    $hostdetails .= "</tr>\n";
  }
  $hostdetails .= "</tbody>\n</table>\n";
}

# Finally, we can write the output... yuck...
open (F, "<$config{hosthtml}") || &Util::HTMLError("Cannot open host template");
while (<F>) {
  s/~hostlist~/$hostlist/;
  s/~hostdetails~/$hostdetails/;
  print;
}
close F;
